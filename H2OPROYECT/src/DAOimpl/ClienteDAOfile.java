/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOimpl;

import dao.ClienteDAO;
import daoexception.ClienteNoValidoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cliente;

/**
 *
 * @author Kristian
 */
public class ClienteDAOfile implements ClienteDAO {

    public static final int lId = 10;
    public static final int lNom = 30;
    public static final int lTel = 10;
    public static final int lDir = 15;

//    private static final String nombreArchivo = "clientes";
//    private static String ruta="";
            
    private static File fichero= new File("..\\clientes.txt");
    @Override
    public void crearCli(Cliente cliente) throws ClienteNoValidoException {
        if (!fichero.exists()) {
            fichero = new File("..\\clientes.txt");            
        } 
        if (validar(cliente)&&!llaveDuplicada(fichero, cliente.getId())) {
            String s = convertirClienteARegistro(cliente);
            
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                writer.write(s);
                writer.close();
            } catch (Exception e) {
                System.out.println("No se puede acceder al archivo");    
            }
        } else {
            throw new ClienteNoValidoException();
        }

    }
      public boolean llaveDuplicada(File tabla, String key) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(tabla));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println("FILE WAS READ!");
            }
            String[] cargas = st.split(";");
            for (int i = 0; i < cargas.length; i++) {
                String[] cc = cargas[i].split(",");
                if (cc[0].equals(key)) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }


    public boolean validar(Cliente cliente) {

        if ((cliente.getNombre() == null || cliente.getId() == null || cliente.getDireccion() == null || cliente.getTelefono() == null)) {
            return false;
        }
        if ((cliente.getNombre().isEmpty() || cliente.getId().isEmpty() || cliente.getDireccion().isEmpty() || cliente.getTelefono().isEmpty())) {
            return false;
        }
        if (cliente.getId().length() > lId || cliente.getNombre().length() > lNom || cliente.getDireccion().length() > lDir || cliente.getTelefono().length() > lTel) {
            return false;
        };
        if (espacios(cliente.getId()) || espacios(cliente.getTelefono())) {
            return false;
        };

        return true;
    }

    public boolean espacios(String a) {
        boolean b = false;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == ' ') {
                b = true;
                return b;
            }
        }
        return b;
    }
    

    @Override
    public Cliente consultarClie(String id) {
        String idAux=rellenar(id,lId);
        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st ;
            while ((st = br.readLine()) != null) {
               System.out.println(st);
            }
            String[] clienteCaracteres = st.split(";");
            for (int i = 0; i < clienteCaracteres.length; i++) {
                Cliente cliente= convertirCaracteresACliente(clienteCaracteres[i]);
                if (cliente.getId().equals(idAux)) {
                    return cliente;
                }
            }

        } catch (Exception e) {
           // e.printStackTrace();
            System.out.println("No se pudo leer el archivo");
        }
        return null;

    }
    private Cliente convertirCaracteresACliente(String cliente) {
        String[] clienteCaracteres = cliente.split(",");
        Cliente clienteObjeto = new Cliente(clienteCaracteres[0],
                clienteCaracteres[1],
                clienteCaracteres[2],
                clienteCaracteres[3]);
        return clienteObjeto;
    }
    
//por rregla de negocio ya se debio haber validado que el cliente a editar si existe, 
    //y solo se manda en el objeto cliente que se va a modificar
    @Override
    public boolean editarCli(Cliente cliente) {       
       StringBuilder ClienteEdit=new StringBuilder();
        boolean b=false;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st ;
            while ((st = br.readLine()) != null) {
               System.out.println(st);
            }
            String[] clienteCaracteres = st.split(";");
            for (int i = 0; i < clienteCaracteres.length; i++) {
                Cliente objcliente= convertirCaracteresACliente(clienteCaracteres[i]);
                if (objcliente.getId().equals(rellenar(cliente.getId(),lId))) {                   
                   ClienteEdit.append(rellenar(cliente.getId(),lId)).append(",");
                   ClienteEdit.append(rellenar(cliente.getNombre(),lNom)).append(",");
                   ClienteEdit.append(rellenar(cliente.getTelefono(),lTel)).append(",");
                   ClienteEdit.append(rellenar(cliente.getDireccion(),lDir)).append(";");                 
                     b= true;
                }else{                    
                    ClienteEdit.append(clienteCaracteres[i]).append(";");                                     
                }}
                fichero.delete();
                fichero= new File("..\\clientes.txt");
                 try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                writer.write(ClienteEdit.toString());
                writer.close();
            } catch (Exception e) {
//                e.printStackTrace();
                     System.out.println("No le pudo leer el archivo");
            }         
                
            } catch (IOException ex) {
            //Logger.getLogger(ClienteDAOfile.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No se pudo leer el archivo");
        }       
       return b;
    }

    

    private String convertirClienteARegistro(Cliente cliente) {
        StringBuilder clienteString = new StringBuilder();

        clienteString.append(rellenar(cliente.getId(),lId)).append(",");
        clienteString.append(rellenar(cliente.getNombre(),lNom)).append(",");
        clienteString.append(rellenar(cliente.getTelefono(),lTel)).append(",");
        clienteString.append(rellenar(cliente.getDireccion(),lDir)).append(";");
        return clienteString.toString();
    }

    public String rellenar(String s, int l) {
        String g = s;         
        if (g.length() < l) {
           int faltan = l - g.length();
            for (int i =0; i < faltan; i++) {
                g += " ";
            }
        }
        return g;
    }
     public Cliente[] clientesTotales() {
      if (!fichero.exists()) {
          return null;
      }
        try 
        {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            if((st = br.readLine())!= null) 
            {
                String[] clienteSt = st.split(";");
                Cliente[] clienteB=new Cliente[clienteSt.length];
                for(int i=0;i<clienteSt.length;i++){
                    clienteB[i]=convertirCaracteresACliente(clienteSt[i]);
                }                
                return clienteB;         
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        
    }

}
