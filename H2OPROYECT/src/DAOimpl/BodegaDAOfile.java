/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOimpl;

import dao.BodegaDAO;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import modelo.Bodega;
import modelo.Producto;

/**
 *
 * @author Kristian
 */
public class BodegaDAOfile implements BodegaDAO {

    private static final int lLote = 7;
    private static final int ltpProduc = 15;
    private static final int lFElab = 8;
    private static final int Lcantidad = 4;
    private static File fichero;

    @Override
    public void entrar(Bodega bodega) {
        if (!fichero.exists()) {
            fichero = new File("..\\Bodega.txt");

        } else {
            if (validar(bodega)) {
                try {
                    StringBuilder bodegaEdit = new StringBuilder();
                    BufferedReader br = new BufferedReader(new FileReader(fichero));
                    String st;
                    while ((st = br.readLine()) != null) {
                        System.out.println(st);
                    }
                    String[] bodegaCaracteres = st.split(";");
                    for (int i = 0; i < bodegaCaracteres.length; i++) {
                        Bodega objBodega = convertirCaracteresABodega(bodegaCaracteres[i]);
                        if ((objBodega.getLote() == bodega.getLote()) && objBodega.getTipoProducto().equals(bodega.getTipoProducto())) {
                            objBodega.setCantidad(objBodega.getCantidad() + bodega.getCantidad());
                            bodegaEdit.append(rellenar(Integer.toString(objBodega.getLote()), lLote)).append(",");
                            bodegaEdit.append(rellenar(objBodega.getTipoProducto(), ltpProduc)).append(",");
                            bodegaEdit.append(rellenar(objBodega.getfElab(), lFElab)).append(",");
                            bodegaEdit.append(rellenar(Integer.toString(objBodega.getCantidad()), Lcantidad)).append(";");

                        } else {
                            bodegaEdit.append(bodegaCaracteres[i]).append(";");

                        }
                    }
                    fichero.delete();
                    fichero = new File("..\\Bodega.txt");
                    try {
                        BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                        writer.write(bodegaEdit.toString());
                        writer.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public boolean validar(Bodega b) {
        if (b.getLote() <= 0 || b.getTipoProducto() == null || b.getfElab() == null || b.getCantidad() <= 0) {
            return false;
        }
        if (Integer.toString(b.getLote()).isEmpty() || b.getTipoProducto().isEmpty() || b.getfElab().isEmpty() || Integer.toString(b.getCantidad()).isEmpty()) {
            return false;
        }
        if (Integer.toString(b.getLote()).length() > lLote || b.getTipoProducto().length() > ltpProduc || b.getfElab().length() > lFElab || Integer.toString(b.getCantidad()).length() > Lcantidad) {
            return false;
        }
        return true;
    }

    public String rellenar(String s, int l) {
        String g = s;
        if (g.length() < l) {
            int faltan = l - g.length();
            for (int i = 0; i < faltan; i++) {
                g += " ";
            }
        }
        return g;
    }

    @Override
    public int consultar(String tipoLicor) {
        if (!fichero.exists()) {
            return -1;
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println(st);
            }
            int sumaCantidadL = 0;
            String[] bodegaCaracteres = st.split(";");
            for (int i = 0; i < bodegaCaracteres.length; i++) {
                Bodega bodega = convertirCaracteresABodega(bodegaCaracteres[i]);
                if (bodega.getTipoProducto().equalsIgnoreCase(tipoLicor)) {
                    sumaCantidadL = sumaCantidadL + bodega.getCantidad(); //revisar si almacena con espacios o ceros
                }
            }
            return sumaCantidadL;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    private Bodega convertirCaracteresABodega(String bodega) {
        String[] bodegaCaracteres = bodega.split(",");
        Bodega bodegaObjeto = new Bodega(
                Integer.parseInt(eliminaEspa(bodegaCaracteres[0])),
                eliminaEspa(bodegaCaracteres[1]),
                eliminaEspa(bodegaCaracteres[2]),
                Integer.parseInt(eliminaEspa(bodegaCaracteres[3])));

        return bodegaObjeto;
    }

    private String eliminaEspa(String s) {
        char[] var = s.toCharArray();
        String varNOespa = "";
        int o = 0;
        while (var[o] != ' ') {
            varNOespa = varNOespa + var[o];
            o++;
        }
        return varNOespa;
    }

    @Override
    public boolean sacar(String tipoLicor ,int cuantos)
    {
        if (!fichero.exists()) {return false;}
        int hay=consultar(tipoLicor);
        if(cuantos<=hay)
        {
            try 
            {
                StringBuilder s=new StringBuilder();
                BufferedReader br = new BufferedReader(new FileReader(fichero));
                String st=br.readLine();
                if(st!= null)
                {
                    int requiere=cuantos;
                    String[] bodegaCaracteres = st.split(";");
                    for(int i=0;i<bodegaCaracteres.length; i++)
                    {
                        Bodega bodega = convertirCaracteresABodega(bodegaCaracteres[i]);
                        if(bodega.getTipoProducto().equalsIgnoreCase(tipoLicor)) 
                        {
                            if(requiere>=bodega.getCantidad())
                            {requiere-=bodega.getCantidad();bodega.setCantidad(0);}
                            else
                            {
                                bodega.setCantidad(bodega.getCantidad()-requiere);
                                requiere=0;
                            }
                            s.append(rellenar(Integer.toString(bodega.getLote()),lLote)).append(",");
                            s.append(rellenar(bodega.getTipoProducto(),ltpProduc)).append(",");
                            s.append(rellenar(bodega.getfElab(),lFElab)).append(",");
                            s.append(rellenar(Integer.toString(bodega.getCantidad()),Lcantidad)).append(";");
                            //lote tipoloicor fecha cantidad
                        }else
                        {s.append(bodegaCaracteres[i]).append(";");}
                    }
                    if(requiere==0)
                    {
                       fichero.delete();
                       fichero = new File("..\\Bodega.txt");
                       BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                       writer.write(s.toString());
                       writer.close();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    
    }
    
    @Override
  public Bodega[] inventarioTotal() {
      if (!fichero.exists()) {
          return null;
      }
        try 
        {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            if((st = br.readLine())!= null) 
            {
                String[] bodegaSt = st.split(";");
                Bodega[] bodegaB=new Bodega[bodegaSt.length];
                for(int i=0;i<bodegaSt.length;i++){
                    bodegaB[i]=convertirCaracteresABodega(bodegaSt[i]);
                }
                
                return bodegaB;         
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        
    }
    

}
