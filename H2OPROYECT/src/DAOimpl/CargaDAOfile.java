/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOimpl;

import static DAOimpl.ClienteDAOfile.lDir;
import static DAOimpl.ClienteDAOfile.lId;
import static DAOimpl.ClienteDAOfile.lNom;
import static DAOimpl.ClienteDAOfile.lTel;
import dao.CargaDAO;
import daoexception.CargaNoValidoException;
import daoexception.ClienteNoValidoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Carga;
import modelo.Cliente;

/**
 *
 * @author Kristian
 */
public class CargaDAOfile implements CargaDAO {

    static final int lConseCarga = 10;
    static final int lconsePedido = 10;
    static final int ltProducto = 20;
    static final int lFecha = 8;
    static final int lHora = 6;
    static final int lVehi = 6;
    static final int ltranspor = 10;
    static final int lRecibe = 10;
    static final int lCantidad = 4;

    private static File fichero;
    public static int conseCarga;

    public boolean llaveDuplicada(File tabla, String key) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(tabla));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println("FILE WAS READ!");
            }
            String[] cargas = st.split(";");
            for (int i = 0; i < cargas.length; i++) {
                String[] cc = cargas[i].split(",");
                if (cc[0].equals(key)) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public void Agregar(Carga carga) throws CargaNoValidoException {
        if (!fichero.exists()) {
            fichero = new File("..\\Pedidos.txt");
            carga.setConsecCarga(0);
        } else {
            conseCarga = conseActual(fichero);
            carga.setConsecCarga(conseCarga);
        }
        if (validar(carga)) {
            String s = convertirCargaARegistro(carga);
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                writer.write(s);
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new CargaNoValidoException();
        }

    }

    private String convertirCargaARegistro(Carga carga) {
        StringBuilder cargaString = new StringBuilder();

        cargaString.append(rellenar(Integer.toString(carga.getConsecCarga()), lConseCarga)).append(",");
        cargaString.append(rellenar(Integer.toString(carga.getConsePedido()), lconsePedido)).append(",");
        cargaString.append(rellenar(carga.getTpProducto(), ltProducto)).append(";");
        cargaString.append(rellenar(carga.getFecha(), lFecha)).append(";");
        cargaString.append(rellenar(carga.getHora(), lHora)).append(";");
        cargaString.append(rellenar(carga.getVehiculo(), lVehi)).append(";");
        cargaString.append(rellenar(carga.getTransportador(), ltranspor)).append(";");
        cargaString.append(rellenar(carga.getRecibe(), lRecibe)).append(";");
        cargaString.append(rellenar(Integer.toString(carga.getCantidad()), lCantidad)).append(";");
        return cargaString.toString();
    }

    public String rellenar(String s, int l) {
        String g = s;
        if (g.length() < l) {
            int faltan = l - g.length();
            for (int i = 0; i < faltan; i++) {
                g += " ";
            }
        }
        return g;
    }

    public boolean validar(Carga carga) {

        if (carga.getTpProducto() == null || carga.getFecha() == null || carga.getHora() == null || carga.getVehiculo() == null || carga.getTransportador() == null || carga.getRecibe() == null || carga.getCantidad() == 0) {
            return false;
        }
        if (carga.getTpProducto().isEmpty() || carga.getFecha().isEmpty() || carga.getHora().isEmpty() || carga.getVehiculo().isEmpty() || carga.getTransportador().isEmpty() || carga.getRecibe().isEmpty()) {
            return false;
        }
        if (Integer.toString(carga.getConsecCarga()).length() > lConseCarga || Integer.toString(carga.getConsePedido()).length() > lconsePedido || carga.getTpProducto().length() > ltProducto || carga.getFecha().length() > lFecha
                || carga.getHora().length() > lHora || carga.getVehiculo().length() > lVehi || carga.getTransportador().length() > ltranspor || carga.getRecibe().length() > lRecibe || Integer.toString(carga.getCantidad()).length() > lCantidad) {
            return false;
        };
        if (espacios(Integer.toString(carga.getCantidad()))) {
            return false;
        }

        return true;
    }

    public boolean espacios(String a) {
        boolean b = false;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == ' ') {
                b = true;
                return b;
            }
        }
        return b;
    }

    public int conseActual(File file) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println("FILE WAS READ!");
            }
            String[] cargas = st.split(";");
            String[] cc = cargas[cargas.length - 1].split(",");
            return (Integer.parseInt(cc[0]) + 1);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    @Override
    public Carga consultar(String consecutivoCarga) {
        if (!fichero.exists()) {
            return null;
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println(st);
            }
            String[] cargaCaracteres = st.split(";");
            for (int i = 0; i < cargaCaracteres.length; i++) {
                Carga carga = convertirCaracteresACarga(cargaCaracteres[i]);
                if (carga.getConsecCarga() == Integer.parseInt(consecutivoCarga)) {
                    return carga;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Carga convertirCaracteresACarga(String carga) {
        String[] cargaCaracteres = carga.split(",");
        Carga cargaObjeto = new Carga(Integer.parseInt(eliminaEspa(cargaCaracteres[0])),
                Integer.parseInt(eliminaEspa(cargaCaracteres[1])),
                cargaCaracteres[2],
                cargaCaracteres[3],
                cargaCaracteres[4],
                cargaCaracteres[5],
                cargaCaracteres[6],
                cargaCaracteres[7],
                Integer.parseInt(eliminaEspa(cargaCaracteres[8]))
        );
        return cargaObjeto;
    }

    private String eliminaEspa(String s) {
        char[] var = s.toCharArray();
        String varNOespa = "";
        int o = 0;
        while (var[o] != ' ') {
            varNOespa = varNOespa + var[o];
            o++;
        }
        return varNOespa;
    }

    @Override
    public boolean modificar(Carga carga) {
          BufferedReader br = null;
          boolean b=false;
            try {
                StringBuilder cargaEdit = new StringBuilder();
                br = new BufferedReader(new FileReader(fichero));
                String st;
                while ((st = br.readLine()) != null) {
                    System.out.println(st);
                }
                String[] cargaCaracteres = st.split(";");
                for (int i = 0; i < cargaCaracteres.length; i++) {
                    Carga objCarga = convertirCaracteresACarga(cargaCaracteres[i]);
                    if (objCarga.getConsecCarga() ==carga.getConsecCarga()) {
                        cargaEdit.append(carga.getConsecCarga()).append(",");
                        cargaEdit.append(carga.getConsePedido()).append(",");
                        cargaEdit.append(carga.getTpProducto()).append(",");
                        cargaEdit.append(carga.getFecha()).append(",");
                        cargaEdit.append(carga.getHora()).append(",");
                        cargaEdit.append(carga.getVehiculo()).append(",");
                        cargaEdit.append(carga.getTransportador()).append(",");
                        cargaEdit.append(carga.getRecibe()).append(",");                        
                        cargaEdit.append(carga.getCantidad()).append(";");
                        b=true;
                    } else {
                        cargaEdit.append(cargaCaracteres[i]).append(";");
                    }
                }
                fichero.delete();
                fichero = new File("..\\Pedido.txt");
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                    writer.write(cargaEdit.toString());
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(PedidoDAOfile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
            Logger.getLogger(PedidoDAOfile.class.getName()).log(Level.SEVERE, null, ex);
            }
            return b;
    }

//    @Override
//    public String[] consultarcargas(String consecutivoPedido) {
//      if (!fichero.exists()) {
//            return null;
//        }
//       String[] cargasCaracteres=null;
//       String[] cargasCliente=null;
//        try {
//            BufferedReader br = new BufferedReader(new FileReader(fichero));
//            String st;
//            
//            while ((st = br.readLine()) != null) {
//                System.out.println(st);
//            }            
//           cargasCaracteres = st.split(";");
//           String cargaAux="";
//           for(int i=0;i<cargasCaracteres.length;i++){
//               if(eliminaEspa(cargasCaracteres[1]).equals(consecutivoPedido)){
//                   cargaAux=cargaAux+","+cargasCaracteres[0];
//               }
//           }
//           return cargasCliente=cargaAux.split(",");
//           
//           
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null ;
//        
//    }
    @Override
    public Carga[] consultarcargas(String consecutivoPedido) {
       if (!fichero.exists()) {
            return null;
        }
       String[] cargaCaracteres=null;
       String[] cargasPedido=null;
       Carga[] objCarga;
       String dt="";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            
            while ((st = br.readLine()) != null) {
                System.out.println(st);
            }            
           cargaCaracteres = st.split(";");
           String pedidosaux="";
           for(int i=0;i<cargaCaracteres.length;i++){
               Carga objtCarga=convertirCaracteresACarga(cargaCaracteres[i]);
               if(objtCarga.getConsePedido()==Integer.parseInt(consecutivoPedido)){
                   dt=dt+cargaCaracteres[i]+";";
               }
           }
            cargasPedido=dt.split(";");
            objCarga=new Carga[cargasPedido.length];
            for(int i=0;i<objCarga.length;i++){
            objCarga[i]=convertirCaracteresACarga(cargasPedido[i]);
            
            }
            return objCarga;
           
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null ;
        
    }
    }


