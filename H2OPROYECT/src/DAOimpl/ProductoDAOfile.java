/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOimpl;

import static DAOimpl.PedidoDAOfile.consePed;
import static DAOimpl.PedidoDAOfile.lconsPed;
import dao.ProductoDao;
import java.io.*;
import daoexception.PedidoNoValidoException;
import daoexception.ProductoNoValidoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import modelo.Pedido;
import modelo.Producto;
import DAOimpl.PedidoDAOfile;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kristian
 */
public class ProductoDAOfile implements ProductoDao
{
    public static final int ltipo=7;
    public static final int lporcentaje=2;
    public static final int lcapacidad= 3;//1.5   en litros ps
    static int lregistro=ltipo+lporcentaje+lcapacidad+3;//2 por las dos comas y el ;
    private static File fichero;
    public static final String path="..\\Pedidos.txt";
    @Override
    public void Agregar(Producto p) throws ProductoNoValidoException 
    {
        if (!fichero.exists())
        {
            fichero=new File(path);
        } else 
        {
            if(!llaveDuplicada(fichero,p.getTipoLicor()))
            {
                if (validar(p))
                {
                    String s=convertirProductoARegistro(p);
                    try 
                    {
                        BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                        writer.write(s);
                        writer.close();
                    } catch (Exception e) 
                    {
                        e.printStackTrace();
                    }
                } else {throw new ProductoNoValidoException();}
            }
        }
    }
    public String rellenar(String s, int l) {
        String g = s;         
        if (g.length() < l) {
           int faltan = l - g.length();
            for (int i =0; i < faltan; i++) {
                g += " ";
            }
        }
        return g;
    } 
    private String convertirProductoARegistro(Producto p)
    {
        StringBuilder a= new StringBuilder();
        a.append(rellenar(p.getTipoLicor(),ltipo)).append(",");
        a.append(rellenar(p.getPorjeAlcohol(),lporcentaje)).append(",");
        a.append(rellenar(p.getCapacidad(),lcapacidad)).append(";");
        return a.toString();
    } 
    public boolean validar(Producto p) 
    {
        if(p.getTipoLicor() == null ||p.getPorjeAlcohol()==null||p.getCapacidad()==null ){return false;}
        if(p.getTipoLicor().isEmpty()||p.getPorjeAlcohol().isEmpty()||p.getCapacidad().isEmpty()){return false;}
        if (p.getTipoLicor().length() > ltipo|| p.getPorjeAlcohol().length() > lporcentaje || p.getCapacidad().length() > lcapacidad ) {return false;}
        return true;
    }
    public boolean llaveDuplicada(File tabla,String key)
    {
        BufferedReader br;
        try 
        {
            br = new BufferedReader(new FileReader(tabla));
            String st;
            while((st=br.readLine())!=null)
            {
                System.out.println("FILE WAS READ!");
            }
            String[] cargas=st.split(";");
            for (int i = 0; i <cargas.length; i++) 
            {
                String[] cc=cargas[i].split(",");
                if(cc[0].equals(key))
                {
                    return true;
                }
            }
        } 
        catch(Exception ex) 
        {
            ex.printStackTrace();
        }
        return false;
    }
    private Producto convertirCaracteresAProducto(String p)
    {
        String[] pp= p.split(",");
        Producto Obj=new Producto(pp[0],pp[1],pp[2]);
        return Obj;
    }
    
    @Override
    public boolean modificar(Producto pro) 
    {
        String tipolicor=pro.getTipoLicor();
        if (fichero.exists())
        {
            try 
            {
                RandomAccessFile raf = new RandomAccessFile(fichero, "rw");
                String s=raf.toString();
                String[] b =s.split(";");
                int registro_numero=-1;
                int i;
                for(i=0;i<b.length;i++)
                {
                    Producto p=convertirCaracteresAProducto(b[i]);
                    if (p.getTipoLicor().equals(tipolicor)) 
                    {registro_numero=i;break;}//el producto a modificar si existe!
                }
                int posarchivo=0;
                if(registro_numero!=-1)//si si existe
                {
                    posarchivo=lregistro*registro_numero;
                    posarchivo+=1;
                    raf.write(b[i].getBytes(),posarchivo,lregistro-1);
                    return true;
                }

            } catch (Exception e) {
                e.printStackTrace();return false;
            }
        }
        return false;
    }


    @Override
    public String[] consultartodo()
    {
        if (!fichero.exists()) {return null;}
        try 
        {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            if((st = br.readLine())!= null) 
            {
                String[] productos = st.split(";");
                return productos;         
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;    
    }

    @Override
    public Producto consultar(String tipolicor)
    {
        if (!fichero.exists()) {return null;}
        try 
        {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            if((st = br.readLine())!= null) 
            {
                String[] productos = st.split(";");
                for (int i=0;i<productos.length;i++)
                {
                    Producto pro=convertirCaracteresAProducto(productos[i]);
                    if (pro.getTipoLicor().equals(tipolicor))
                    {
                        return pro;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
}
