/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Kristian
 */
public class Carga {
    private int consecCarga;
    private int  consePedido;
    private String tpProducto;
    private String fecha;
    private String hora;
    private String vehiculo;
    private String transportador;
    private String recibe;
    private int cantidad;

    public Carga(int consecCarga, int consePedido, String tpProducto, String fecha, String hora, String vehiculo, String transportador, String recibe, int cantidad) {
        this.consecCarga = consecCarga;
        this.consePedido = consePedido;
        this.tpProducto = tpProducto;
        this.fecha = fecha;
        this.hora = hora;
        this.vehiculo = vehiculo;
        this.transportador = transportador;
        this.recibe = recibe;
        this.cantidad = cantidad;
    }
    @Override
    public String toString() 
    {
        return "Carga"+this.consecCarga+ "del pedido" +consePedido+ " tipo:" + tpProducto
        + "fecha:" + fecha + " hora :" +hora+" vehiculo: "
        +vehiculo+" transportador: "+transportador+" recibe: "+recibe+" cantidad: "+cantidad;
    }
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getConsePedido() {
        return consePedido;
    }

    public void setConsePedido(int consePedido) {
        this.consePedido = consePedido;
    }

    public int getConsecCarga() {
        return consecCarga;
    }

    public void setConsecCarga(int cosecCarga) {
        this.consecCarga = cosecCarga;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getTransportador() {
        return transportador;
    }

    public void setTransportador(String transportador) {
        this.transportador = transportador;
    }

    public String getRecibe() {
        return recibe;
    }

    public void setRecibe(String recibe) {
        this.recibe = recibe;
    }

    public String getTpProducto() {
        return tpProducto;
    }

    public void setTpProducto(String tpProducto) {
        this.tpProducto = tpProducto;
    }
    
    
}
