/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Kristian
 */
public class Producto {
    private String tipoLicor;
    private String PorjeAlcohol;
    private String capacidad;

    public Producto(String tipoLicor, String PorjeAlcohol, String capacidad) {
        this.tipoLicor = tipoLicor;
        this.PorjeAlcohol = PorjeAlcohol;
        this.capacidad = capacidad;
    }

    @Override
    public String toString() {
        return  tipoLicor+" "+PorjeAlcohol+"% "+capacidad+"Litros";
    }
    
    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public String getTipoLicor() {
        return tipoLicor;
    }

    public void setTipoLicor(String tipoLicor) {
        this.tipoLicor = tipoLicor;
    }

    public String getPorjeAlcohol() {
        return PorjeAlcohol;
    }

    public void setPorjeAlcohol(String PorjeAlcohol) {
        this.PorjeAlcohol = PorjeAlcohol;
    }
}
