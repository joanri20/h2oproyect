/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BsnImplementacion;

import Bsn.PedidoBsn;
import DAOimpl.PedidoDAOfile;
import dao.PedidoDAO;
import daoexception.PedidoNoValidoException;
import modelo.Pedido;

/**
 *
 * @author Kristian
 */

public class PedidoBsnImpl implements PedidoBsn {
PedidoDAO pedidoDAO;

    public PedidoBsnImpl() {
            pedidoDAO= new PedidoDAOfile();
    }

    @Override
    public void Agregar(Pedido p) {
    if(p.getConsePed()!=0000000000){System.out.println("No se puede guardar por mal reporte de pago");
        }else{
        try{pedidoDAO.Agregar(p);
        }catch(PedidoNoValidoException  pedidoNovalidoException){
            System.out.println("Pedido no valido por campos o llave repetida ");
        
        }
            
        }
    }

    @Override
    public Pedido consultar(String consecutivopedido) {
        if(pedidoDAO.consultar(consecutivopedido)!=null ){
           return pedidoDAO.consultar(consecutivopedido);
       }
       return null;
    }
    

    @Override
    public boolean modificar(Pedido p) {
      return pedidoDAO.modificar(p);  
    }

    @Override
    public Pedido[] consultarPedidos(String idcliente) {
       return pedidoDAO.consultarPedidos(idcliente);
    }

  
    
}
