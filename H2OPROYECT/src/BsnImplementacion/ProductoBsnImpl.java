/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BsnImplementacion;

import Bsn.ProductoBsn;
import DAOimpl.ProductoDAOfile;
import dao.ProductoDao;
import daoexception.ProductoNoValidoException;
import modelo.Producto;

/**
 *
 * @author Dan
 */
public class ProductoBsnImpl implements ProductoBsn
{
    ProductoDao pp;
    ProductoBsnImpl (){pp=new ProductoDAOfile();}
    public boolean Agregar(Producto p)
    {
      if(p.getTipoLicor().equals("etilico"))
      {
          return false;
      }else
      {
         try{ pp.Agregar(p);}catch(ProductoNoValidoException e){return false;}
      }
      return true;
    }

    @Override
    public Producto consultar(String tipolicor)
    {
            Producto temp=pp.consultar(tipolicor);
            if(temp!=null){return temp;}return null;
    }

    @Override
    public boolean modificar(Producto pro) {
        if(pp.modificar(pro)){return true;}
        return false;
    }

    @Override
    public String[] consultartodo() {
        return pp.consultartodo();
    }
}
