/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BsnImplementacion;

import Bsn.CargaBsn;
import DAOimpl.CargaDAOfile;
import dao.CargaDAO;
import daoexception.CargaNoValidoException;
import modelo.Carga;

/**
 *
 * @author Kristian
 */
public class CargasBsnImpl implements CargaBsn {
CargaDAO cargaDAO;
    public CargasBsnImpl() {
        cargaDAO= new CargaDAOfile();
    }   
    @Override
    public void Agregar(Carga c) {
       if(c.getConsecCarga()!=0000000000){System.out.println("No se puede guardar por mal consecutivo");
        }else{
        try{cargaDAO.Agregar(c);
        }catch(CargaNoValidoException  cargaNovalidoException){
            System.out.println("Carga no valido por campos o llave repetida ");
        
        }
    }
    }
    @Override
    public Carga consultar(String consecutivoCarga) {
      if(cargaDAO.consultar(consecutivoCarga)!=null ){
           return cargaDAO.consultar(consecutivoCarga);
       }
       return null;
    }
    @Override
    public boolean modificar(Carga c) {
        return cargaDAO.modificar(c); 
    }
    @Override
    public Carga[] consultarcargas(String consecutivoPedido) {
        return cargaDAO.consultarcargas(consecutivoPedido);
    }

    
}
