/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Bsn.BodegaBsn;
import Bsn.CargaBsn;
import Bsn.ClienteBsn;
import Bsn.PedidoBsn;
import BsnImplementacion.BodegaBsnImpl;
import BsnImplementacion.CargasBsnImpl;
import BsnImplementacion.ClientesBsnImpl;
import BsnImplementacion.PedidoBsnImpl;
import daoexception.CargaNoValidoException;
import java.util.List;
import java.util.Scanner;
import modelo.Carga;
import modelo.Cliente;
import modelo.Pedido;
import modelo.Producto;
import vista.Vista;

/**
 *
 * @author Dan
 */
public class controlador
{
    private static CargaBsn car;
    private static ClienteBsn cli;
    private static BodegaBsn bo;
    private static PedidoBsn ped;
    private static ProductoBsn pro;
    
    
    public controlador()
    {
        bo=new BodegaBsnImpl();
        car=new CargasBsnImpl();
        cli=new ClientesBsnImpl();
        ped=new PedidoBsnImpl();
        // falta la bsn impl de producto
    }
    
    public static void main(String args[])
    {
        int a,b,i;
        String c,d,e,f,g,h;
        controlador controlador=new controlador();
        Scanner l=new Scanner(System.in);
        String op="";
        Vista.menuppal();
        String temp;
        while(!op.equals("7"))
        {
            op=l.next();
            switch(op)
            {
                case"1"://bodega
                    Vista.menuBodega();
                    temp=l.next();
                    switch(temp)
                    {
                        case "1":
                        System.out.println("tipo de producto:");
                        String id=l.next();
                        break;
                    }
                    break;
                case "2"://carga
                    Vista.menu("Carga");
                    temp=l.next();
                    while(!temp.equals("0"))
                    {
                        switch(temp)
                        {
                            case "1"://consultar
                                System.out.println("Entre concecutivo carga:");
                                temp=l.next();
                                Carga carga=car.consultar(temp);
                                if(carga!=null)
                                {
                                    System.out.println(carga.toString());
                                }else
                                {
                                    System.out.println("La carga no existe aun!");
                                }
                                break;
                            case "2"://modificar
                                System.out.println("INGRESE \n concecutivo de Carga:");
                                a=l.nextInt();
                                System.out.println("consecutivo pedido:");
                                b=l.nextInt();
                                System.out.println("Tipo de producto:");
                                c=l.next();
                                System.out.println("fecha:");
                                d=l.next();
                                System.out.println("hora:");
                                e=l.next();
                                System.out.println("vehiculo:");
                                f=l.next();
                                System.out.println("transportador:");
                                g=l.next();
                                System.out.println("recibe:");
                                h=l.next();
                                System.out.println("cantidad:");
                                i=l.nextInt();
                                Carga carg=new Carga(a,b,c,d,e,f,g,h,i);
                                boolean es=car.modificar(carg);
                                if(es){System.out.println("CARGA MODIFICADA");}else
                                {System.out.println("NO SE PUDO MODIFICAR LA CARGA");}
                                break;
                            case "3"://ingresar
                                System.out.println("INGRESE \n concecutivo de Carga:");
                                a=l.nextInt();
                                System.out.println("consecutivo pedido:");
                                b=l.nextInt();
                                System.out.println("Tipo de producto:");
                                c=l.next();
                                System.out.println("fecha:");
                                d=l.next();
                                System.out.println("hora:");
                                e=l.next();
                                System.out.println("vehiculo:");
                                f=l.next();
                                System.out.println("transportador:");
                                g=l.next();
                                System.out.println("recibe:");
                                h=l.next();
                                System.out.println("cantidad:");
                                i=l.nextInt();
                                carg=new Carga(a,b,c,d,e,f,g,h,i);
                                car.Agregar(carg);
                                break;
                            case "4"://todos
                                System.out.println
                                ("INGRESE \n consecutivo pedido al que pertenece la carga deseada:");
                                c=l.next();
                                Carga[] ccc=car.consultarcargas(c);
                                for (i = 0; i <ccc.length; i++)
                                {System.out.println(ccc[i].toString()); }


                        }
                    }
                    break;
                case "3"://cliente
                    Vista.menu("Cliente");
                    temp=l.next();
                    while(!temp.equals("0"))
                    {
                        switch(temp)
                        {
                            case "1"://consultar
                                System.out.println("Entre id cliente:");
                                temp=l.next();
                                Cliente client=cli.consultar(temp);
                                if(client!=null)
                                {
                                    System.out.println(client.toString());
                                }else
                                {
                                    System.out.println("el cliente no existe!");
                                }
                                break;
                            case "2"://modificar
                                //public Cliente(String id,nombre,telefono,direccion);
                                System.out.println("INGRESE para modificar \n id:");
                                c=l.next();
                                System.out.println("nombre:");
                                d=l.next();
                                System.out.println("telefono:");
                                e=l.next();
                                System.out.println("direccion:");
                                f=l.next();
                                Cliente cliente=new Cliente(c,d,e,f);
                                boolean es=cli.modificar(cliente);
                                if(es){System.out.println("CLIENTE MODIFICADo=O");}else
                                {System.out.println("NO SE PUDO MODIFICAR EL CLIENTE");}
                                break;
                            case "3"://ingresar
                                System.out.println("INGRESE \n id cliente:");
                                c=l.next();
                                System.out.println("nombre:");
                                f=l.next();
                                System.out.println("telefono:");
                                d=l.next();
                                System.out.println("direccion:");
                                e=l.next();
                                cliente=new Cliente(c,f,d,e);
                                cli.Crear(cliente);
                                break;
                            case "4"://todos
                                Cliente[] all=cli.inventarioTotal();
                                for (i = 0; i <all.length; i++)
                                {System.out.println(all[i].toString()); }
                        }
                    }
                    break;
                case "4"://pedido
                    Vista.menu("pedido");
                    temp=l.next();
                    while(!temp.equals("0"))
                    {
                        switch(temp)
                        {
                            case "1"://consultar
                                System.out.println("Entre consecutivo pedido:");
                                temp=l.next();
                                Pedido p=ped.consultar(temp);
                                if(p!=null)
                                {
                                    System.out.println(p.toString());
                                }else
                                {
                                    System.out.println("el pedido no existe!");
                                }
                                break;
                            case "2"://modificar
                                System.out.println("INGRESE para modificar \n consecutivo pedido:");
                                int beta=l.nextInt();
                                System.out.println("id cliente:");
                                String gama=l.next();
                                System.out.println("tipo E/I:");
                                c=l.next();
                                System.out.println("fecha de envio:");
                                d=l.next();
                                System.out.println("hora:");
                                e=l.next();
                                System.out.println("estado:");
                                f=l.next();
                                System.out.println("valor");
                                g=l.next();
                                System.out.println("iva");
                                h=l.next();
                                Pedido perf=new Pedido(beta,gama,c.charAt(0),d,e,f,g,h);
                                boolean es=ped.modificar(perf);
                                if(es){System.out.println("Pedido MODIFICADo=O");}else
                                {System.out.println("NO SE PUDO MODIFICAR EL PEDIDO");}
                                break;
                            case "3"://ingresar
                                System.out.println("INGRESE \n consecutivo pedido:");
                                beta=l.nextInt();
                                System.out.println("id cliente:");
                                gama=l.next();
                                System.out.println("tipo E/I:");
                                c=l.next();
                                System.out.println("fecha de envio:");
                                d=l.next();
                                System.out.println("hora:");
                                e=l.next();
                                System.out.println("estado:");
                                f=l.next();
                                System.out.println("valor");
                                g=l.next();
                                System.out.println("iva");
                                h=l.next();
                                perf=new Pedido(beta,gama,c.charAt(0),d,e,f,g,h);
                                ped.Agregar(perf);
                                break;
                            case "4"://todos
                                System.out.println("id cliente:");
                                gama=l.next();
                                Pedido[] alll=ped.consultarPedidos(gama);
                                for (i=0;i<alll.length; i++)
                                {System.out.println(alll[i].toString()); }
                        }
                    }
                    break;    
                    case "5"://producto
                    System.out.println("FALTA HACER LA LOGICA CONTROLADOR DE PRODUCTO");
                    break;
                    case "6"://generar reportes
                    break;          
            }
        }
        System.out.println("Bai");
    }
}
