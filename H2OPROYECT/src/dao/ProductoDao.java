/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import daoexception.ProductoNoValidoException;
import daoexception.PedidoNoValidoException;
import modelo.Pedido;
import modelo.Producto;

/**
 *
 * @author Dan
 */
public interface ProductoDao {
    void Agregar(Producto p)throws ProductoNoValidoException;
    Producto consultar(String tipolicor);
    boolean modificar(Producto pro);
    String[] consultartodo();
}
