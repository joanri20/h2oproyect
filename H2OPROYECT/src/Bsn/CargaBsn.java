/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bsn;

import modelo.Carga;

/*digo que  en los dao si 
    no puede leer en el dao tira una ecxeption quedebe ser la
    misma que ya habrramos definido , la de que no pudo
    leer el archivo .luego aqui en la capa del 
    negocio el catchea la eception y de ahi supongo que 
    tira la misma para ya el controlador..aqui no pueden 
    haber pedidos duplicados , eso no pasa.YA
    DEFINIR SI CONTROLAMOS EL EXITO O NO POR EXCEPTIONS O POR 
    RETURN ES MEJOR VERLO CUANDO IMPLEMENTAMOS cada METODO
    */
public interface CargaBsn 
{
    void Agregar(Carga c);
    Carga consultar (String consecutivocarga);
    boolean modificar(Carga c);
    Carga[] consultarcargas(String consecutivopedido);
}
