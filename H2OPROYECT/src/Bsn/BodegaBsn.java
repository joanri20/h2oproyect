/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bsn;

import modelo.Bodega;
import modelo.Producto;

/**
 *
 * @author Dan
 */
public interface BodegaBsn 
{
    int consultar(String lote);//cuantos hay
    boolean sacar(String tpLicor ,int cuantos);
    void entrar(Bodega bodega);
    Bodega[] consultarBodega();
}
